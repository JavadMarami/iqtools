function varargout = iqcorrmgmt(varargin)
% IQCORRMGMT MATLAB code for iqcorrmgmt.fig
%      IQCORRMGMT, by itself, creates a new IQCORRMGMT or raises the existing
%      singleton*.
%
%      H = IQCORRMGMT returns the handle to a new IQCORRMGMT or the handle to
%      the existing singleton*.
%
%      IQCORRMGMT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IQCORRMGMT.M with the given input arguments.
%
%      IQCORRMGMT('Property','Value',...) creates a new IQCORRMGMT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before iqcorrmgmt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to iqcorrmgmt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help iqcorrmgmt

% Last Modified by GUIDE v2.5 17-Oct-2014 20:36:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @iqcorrmgmt_OpeningFcn, ...
                   'gui_OutputFcn',  @iqcorrmgmt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before iqcorrmgmt is made visible.
function iqcorrmgmt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to iqcorrmgmt (see VARARGIN)

% Choose default command line output for iqcorrmgmt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% update GUI
% update the popupmenu for SParameter selection
try
    acs = load(iqampCorrFilename());
    if (isfield(acs, 'sparamFile'))
        sp = read(rfdata.data, acs.sparamFile);
        if (~isempty(sp))
            rows = size(sp.S_Parameters, 1);
            cols = size(sp.S_Parameters, 2);
            pList = cell(rows*cols, 1);
            for i = 1:rows
                for j = 1:cols
                    pList{(i-1)*cols+j} = sprintf('S%d%d', i, j);
                end
            end
        end
        set(handles.popupmenuSParamSelect, 'String', pList);
    end
catch
end
updateFilename(handles);
updateAxes(handles);
% UIWAIT makes iqcorrmgmt wait for user response (see UIRESUME)
% uiwait(handles.iqcorrmgmt);


% --- Outputs from this function are returned to the command line.
function varargout = iqcorrmgmt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbuttonClose.
function pushbuttonClose_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close();


function editFilename_Callback(hObject, eventdata, handles)
% hObject    handle to editFilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFilename as text
%        str2double(get(hObject,'String')) returns contents of editFilename as a double


% --- Executes during object creation, after setting all properties.
function editFilename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonImport.
function pushbuttonImport_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonImport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% process the following types of files:
%  .csv  VSA exported trace
%  .mat  VSA exported trace
%  .mat  M8195A Calibration file
%
[filename pathname] = uigetfile('*.mat;*.csv');
if (filename ~= 0)
    hMsgbox = msgbox('Importing file...', 'Importing file...', 'replace');
    try
        [path, name, ext] = fileparts(filename);
        if (strcmp(ext, '.csv') || strcmp(ext, '.CSV'))
            % VSA equalizer trace file with header
            % expect to get values for XStart, XDelta and Y
            try
                f = fopen(strcat(pathname, filename), 'r');
                a = fgetl(f);
                cnt = 0;
                clear Y;
                XStart = 0;
                XDelta = 0;
                while (a ~= -1)
%                    fprintf('%s\n', a);
                    % process pairs
                    if (cnt > 0)
                        [val, readCnt] = sscanf(a, '%g,%g'); % US style
                        if (readCnt < 2)
                            [val, readCnt] = sscanf(regexprep(a, ',', '.'), '%g;%g'); % German style
                            if (readCnt < 2)
                                errordlg({'unexpected number format in CSV file: ' a});
                                return;
                            end
                        end
                        Y(cnt,1) = complex(val(1), val(2));
                        cnt = cnt + 1;
                    else
                        [tok, remain] = strtok(a, ',;');
                        switch (tok)
                            case 'Y'
                                cnt = 1;
                            case 'XDelta'
                                XDelta = sscanf(regexprep(remain(2:end), ',', '.'), '%g');
                            case 'XStart'
                                XStart = sscanf(regexprep(remain(2:end), ',', '.'), '%g');
                        end
                    end
                    a = fgetl(f);
                end
                fclose(f);
                numPts = cnt - 1;
                % frequency vector
                freq = linspace(XStart, XStart + (numPts - 1) * XDelta, numPts);
                % allow frequency shift
                result = inputdlg('Shift Frequency (use negative values to shift from RF to baseband)', 'Shift Frequency', 1, {'0'});
                if (~isempty(result))
                    freq = freq + eval(result{1});
                    updateAmpCorr(handles, freq, Y);
                end
            catch ex
                errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)]});
            end
        else
            % process .MAT file - could be either VSA equalizer trace or
            % M8195A calibration file
            eq = load(strcat(pathname, filename));
            if (isfield(eq, 'Cal')) % M8195A Calibration file
                % ask user to select a channel
                result = inputdlg('Please select a channel', 'Please select a channel', 1, {'1'});
                if (~isempty(result))
                    ch = eval(result{1});
                    freq = 1e9 * eq.Cal.Frequency_MT;
                    amp = 10 .^ (eq.Cal.AmplitudeResponse_MT(:,ch) / 20);
                    phi = [0; eq.Cal.AbsPhaseResponse_MT(:,ch)] * pi / 180;
                    updateAmpCorr(handles, freq, amp .* exp(j * phi));
                end
            elseif (~isfield(eq, 'Y') || ~isfield(eq, 'XStart') || ~isfield(eq, 'XDelta')) % VSA trace file
                errordlg('Invalid correction file format. Expected variables "Y" and "XStart" in the file');
            else
                % frequency vector
                freq = linspace(eq.XStart, eq.XStart + (length(eq.Y) - 1) * eq.XDelta, length(eq.Y));
                % allow frequency shift
                result = inputdlg('Shift Frequency (use negative values to shift from RF to baseband)', 'Shift Frequency', 1, {'0'});
                if (~isempty(result))
                    freq = freq + eval(result{1});
                    updateAmpCorr(handles, freq, eq.Y);
                end
            end
        end
    catch ex
        errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)]});
    end
    try
        close(hMsgbox);
    catch
    end
end



function updateAmpCorr(handles, freq, Y)
% calculate response in dB
Ydb = 20*log10(abs(Y));
% set up ampCorr structure
clear ampCorr;
ampCorr(:,1) = freq(1:end);
ampCorr(:,2) = -Ydb;
ampCorr(:,3) = 1 ./ Y;
% get the filename
ampCorrFile = iqampCorrFilename();
clear acs;
% try to load ampCorr file - be graceful if it does not exist
try
    acs = load(ampCorrFile);
catch
end
acs.ampCorr = ampCorr;
% and save
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);



function updatePerChannelCorr(handles, freq, Y)
% set up perChannelCorr structure
clear perChannelCorr;
perChannelCorr(:,1) = freq(1:end);
perChannelCorr(:,2:size(Y,2)+1) = 1 ./ Y;
% get the filename
ampCorrFile = iqampCorrFilename();
clear acs;
% try to load ampCorr file - be graceful if it does not exist
try
    acs = load(ampCorrFile);
catch
end
acs.perChannelCorr = perChannelCorr;
% and save
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);



% --- Executes on button press in pushbuttonExport.
function pushbuttonExport_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonExport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname, filterindex] = uiputfile({...
    '.mat', 'MATLAB file (*.mat)'; ...
    '.csv', 'CSV file (*.csv)'}, ...
    'Save Frequency Response As...');
if (filename ~= 0)
    try
        corrfilename = iqampCorrFilename();
        load(corrfilename);
        if (size(ampCorr,2) <= 2)  % no complex correction available
            ampCorr(:,3) = 10.^(ampCorr(:,2)/20);
        end
        % store frequency response = inverse correction
        Y = 1 ./ ampCorr(:,3);
        switch (filterindex)
            case 1
                XStart = ampCorr(1,1);
                XDelta = ampCorr(2,1) - ampCorr(1,1);
                save(strcat(pathname, filename), 'XStart', 'XDelta', 'Y');
            case 2
                f = fopen(strcat(pathname, filename), 'w');
                fprintf(f, sprintf('XStart;%g\n', ampCorr(1,1)));
                fprintf(f, sprintf('XDelta;%g\n', ampCorr(2,1) - ampCorr(1,1)));
                fprintf(f, sprintf('Y\n'));
                for i=1:size(ampCorr,1)
                    fprintf(f, sprintf('%g;%g\n', real(Y(i)), imag(Y(i))));
                end
                fclose(f);
        end
    catch ex
        errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)]});
    end
end


function updateFilename(handles)
filename = iqampCorrFilename();
set(handles.editFilename, 'String', filename);


function updateAxes(handles)
acs = [];
try
    acs = load(iqampCorrFilename());
catch
end
try
    if (isempty(acs)) % if ampCorr does not exist, create a dummy file
        acs.ampCorr = [1e6 0 1; 2e6 0 1];
    end
    if (isfield(acs, 'ampCorrMode'))
        ampCorrMode = acs.ampCorrMode;
    else
        ampCorrMode = -1;   % old style: de-embed
    end
    idx = [1 3 2];
    set(handles.popupmenuStdMode, 'Value', idx(ampCorrMode+2));
    % check if we use an S-Parameter file
    if (isfield(acs, 'sparamFile'))
        [~, name, ext] = fileparts(acs.sparamFile);
        set(handles.editSParamFile, 'String', strcat(name, ext));
        if (isfield(acs, 'sparamMode'))
            sparamMode = acs.sparamMode;
        else
            sparamMode = -1;
        end
    else
        set(handles.editSParamFile, 'String', '');
        sparamMode = 0;
    end
    idx = [1 3 2];
    set(handles.popupmenuSParamMode, 'Value', idx(sparamMode + 2));
    if (isfield(acs, 'sparamCutoff'))
        cutoff = acs.sparamCutoff;
        set(handles.editSParamCutoff, 'String', sprintf('%g', cutoff));
    else
        set(handles.editSParamCutoff, 'String', '');
    end
    
    [ampCorr, perChannelCorr] = iqcorrection([]);
    phase = -1 * 180 / pi * unwrap(angle(ampCorr(:,3)));
    % if phase is *very* small, plot it as zero to avoid confusion...
    if (max(abs(phase)) < 1e-10)
        phase = zeros(size(phase,1), size(phase,2));
    end
    axes(handles.axes1);
    %plot(ampCorr(:,1), -1*ampCorr(:,2), '.-');
    func1 = @(x,y) plot(x,y,'Linewidth',2);
    [ax, ~, ~] = plotyy(ampCorr(:,1), -1*ampCorr(:,2), ampCorr(:,1), phase, func1);
    legend({'Magnitude', 'Phase'});
    xlabel('Frequency (Hz)');
    ylabel('Loss (dB)');
    axes(ax(2));
    ylabel('Phase (degrees)');
    title = get(gca(), 'Title');
    set(title, 'String', 'Complex Frequency/Phase Response');
    set(title, 'FontWeight', 'bold');
    grid on;
    axes(handles.axes2);
    if (size(perChannelCorr, 2) >= 2)  % per channel correction
        y1 = -20.*log10(abs(perChannelCorr(:,2:end)));
        y2 = -180 / pi * unwrap(angle(perChannelCorr(:,2:end)));
    else
        perChannelCorr = [0; 1e9];
        y1 = zeros(2,1);
        y2 = zeros(2,1);
    end
    func1 = @(x,y) plot(x,y,'Linewidth',2);
    [ax, ~, ~] = plotyy(perChannelCorr(:,1), y1, perChannelCorr(:,1), y2, func1);
    if (size(perChannelCorr,2) > 2)
        legend({'Magn./I' 'Magn./Q' 'Phase/I' 'Phase/Q'});
    else
        legend({'Magnitude' 'Phase'});
    end
    xlabel('Frequency (Hz)');
    ylabel('Loss (dB)');
    axes(ax(2));
    ylabel('Phase (degrees)');
    title = get(gca(), 'Title');
    set(title, 'String', 'Per Channel Frequency/Phase Response');
    set(title, 'FontWeight', 'bold');
    grid on;
catch ex
    errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)]});
end



% --- Executes on button press in pushbuttonDisplaySum.
function pushbuttonDisplaySum_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonDisplaySum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbuttonDisplaySParam.
function pushbuttonDisplaySParam_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonDisplaySParam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenuSParamMode.
function popupmenuSParamMode_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuSParamMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuSParamMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuSParamMode
val = get(handles.popupmenuSParamMode, 'Value');
idx = [-1 1 0];  % de-embed, embed, do nothing
ampCorrFile = iqampCorrFilename();
acs = load(ampCorrFile);
acs.sparamMode = idx(val);
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);


% --- Executes during object creation, after setting all properties.
function popupmenuSParamMode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuSParamMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editSParamFile_Callback(hObject, eventdata, handles)
% hObject    handle to editSParamFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSParamFile as text
%        str2double(get(hObject,'String')) returns contents of editSParamFile as a double
msgbox('Please use the "..." button to change the filename');

% --- Executes during object creation, after setting all properties.
function editSParamFile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSParamFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonLoadSParamFile.
function pushbuttonLoadSParamFile_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonLoadSParamFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ampCorrFile = iqampCorrFilename();
try
    acs = load(ampCorrFile);
catch
    acs.ampCorr = [1e6 0 1; 2e6 0 1];
end
% change to the directory where the previous S-parameter file was located
if (isfield(acs, 'sparamFile'))
    oldWD = cd();
    try
        [path file ext] = fileparts(acs.sparamFile);
        cd(path);
    catch   % if the directory does not exist, simply ignore the error
    end
else
    oldWD = [];
end
% get the filename
[filename pathname] = uigetfile('*.s2p;*.s4p;*.s6p');
% and change dir back to where we were before
if (~isempty(oldWD))
    cd(oldWD);
end
if (filename ~= 0)
    try
        % check if the file can be read
%        sp = reads4p(strcat(pathname, filename));
        sp = read(rfdata.data, strcat(pathname, filename));
        if (~isempty(sp))
            rows = size(sp.S_Parameters, 1);
            cols = size(sp.S_Parameters, 2);
            pList = cell(rows*cols, 1);
            for i = 1:rows
                for j = 1:cols
                    pList{(i-1)*cols+j} = sprintf('S%d%d', i, j);
                end
            end
        end
        set(handles.popupmenuSParamSelect, 'String', pList);
        % select a default SParameter: S21, S31, S21 for 2/4/6 port file
        defVal = [1 1; 2 1; 2 1; 2 1; 2 1; 2 1];
        set(handles.popupmenuSParamSelect, 'Value', (defVal(rows,1)-1)*cols + defVal(rows,2));
        acs.selectedSParam = [defVal(rows,1) defVal(rows,2)];
        acs.sparamFile = strcat(pathname, filename);
        % if it was unused, set to de-embed to avoid confusion
        if (~isfield(acs, 'sparamMode') || acs.sparamMode == 0)
            acs.sparamMode = -1;
        end
        save(ampCorrFile, '-struct', 'acs');
        updateAxes(handles);
    catch ex
        msgbox(ex.message);
    end
end


% --- Executes on selection change in popupmenuStdMode.
function popupmenuStdMode_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuStdMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuStdMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuStdMode
val = get(handles.popupmenuStdMode, 'Value');
idx = [-1 1 0];  % de-embed, embed, do nothing
ampCorrFile = iqampCorrFilename();
acs = load(ampCorrFile);
acs.ampCorrMode = idx(val);
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);


% --- Executes during object creation, after setting all properties.
function popupmenuStdMode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuStdMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editSParamCutoff_Callback(hObject, eventdata, handles)
% hObject    handle to editSParamCutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSParamCutoff as text
%        str2double(get(hObject,'String')) returns contents of editSParamCutoff as a double
ampCorrFile = iqampCorrFilename();
acs = load(ampCorrFile);
try
    acs.sparamCutoff = evalin('base', get(handles.editSParamCutoff, 'String'));
catch
    acs.sparamCutoff = 0;
end
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);

% --- Executes during object creation, after setting all properties.
function editSParamCutoff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSParamCutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on selection change in popupmenuSParamSelect.
function popupmenuSParamSelect_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuSParamSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuSParamSelect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuSParamSelect
vals = cellstr(get(handles.popupmenuSParamSelect, 'String'));
val = vals{get(handles.popupmenuSParamSelect, 'Value')};
sp = sscanf(val, 'S%d');
row = floor(sp/10);
col = sp - 10*row;
ampCorrFile = iqampCorrFilename();
acs = load(ampCorrFile);
acs.selectedSParam = [row col];
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);


% --- Executes during object creation, after setting all properties.
function popupmenuSParamSelect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuSParamSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonStoreAsStd.
function pushbuttonStoreAsStd_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonStoreAsStd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
corr = iqcorrection([]);
% get the filename
ampCorrFile = iqampCorrFilename();
% and save
acs = load(ampCorrFile);
acs.ampCorr = corr;
acs.sparamMode = 0;     % don't double embed/deembed
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);


% --- Executes on button press in pushbuttonReadAWGCal.
function pushbuttonReadAWGCal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonReadAWGCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
arbConfig = loadArbConfig();
if (~isempty(arbConfig) && ~isempty(strfind(arbConfig.model, 'M8195A_Rev1')))
    if (isfield(arbConfig, 'M8195Acorrection') && arbConfig.M8195Acorrection ~= 0)
        errordlg('Please turn off "M8195A built-in corrections" in the configuration window if you want to use the corrections here. Otherwise you will apply the corrections twice');
        return;
    end
    chs = inputdlg({'Correction for "I" from channel: ', 'Correction for "Q" from channel: '}, ...
        'Calibration data set from which channels should be used?', 1, {'1', '3'});
    if (~isempty(chs))
        f = iqopen(arbConfig);
        if (~isempty(f))
            try
                clear cplxCorr;
                for i=1:2
%                    if (i == 1)
%                        a = '0,1,0,1e9,0.9,-0.1,2e9,0.8,-0.2,3e9,0.7,-0.3';
%                    else
%                        a = '0,1,0,1e9,0.8,-0.2,2e9,0.6,-0.4,3e9,0.4,-0.6';
%                    end
                    a = query(f, sprintf('CHAR%d?', str2double(chs{i})));
                    v = sscanf(strrep(strrep(a, '"', ''), ',', ' '), '%g');
                    v = reshape(v, 3, length(v)/3)';
                    freq = v(:,1);
                    cplxCorr(:,i) = v(:,2) .* exp(j * v(:,3));
                end
                updatePerChannelCorr(handles, freq, cplxCorr);
            catch ex
                errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)]});
            end
        end
    end
else
    errordlg({'Only the M8195A supports built-in correction data'});
end


% --- Executes on button press in pushbuttonClearPerChannelCorr.
function pushbuttonClearPerChannelCorr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonClearPerChannelCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ampCorrFile = iqampCorrFilename();
clear acs;
% try to load ampCorr file - be graceful if it does not exist
try
    acs = load(ampCorrFile);
catch
end
acs.perChannelCorr = [];
acs = rmfield(acs, 'perChannelCorr');
% and save
save(ampCorrFile, '-struct', 'acs');
updateAxes(handles);


% --- Executes on button press in pushbuttonClearCplx.
function pushbuttonClearCplx_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonClearCplx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg('Do you really want to delete the complex pre-distortion?', 'Delete complex pre-distortion', 'Yes', 'No', 'No');
if (strcmp(answer, 'Yes'))
    ampCorrFile = iqampCorrFilename();
    clear acs;
    % try to load ampCorr file - be graceful if it does not exist
    try
        acs = load(ampCorrFile);
    catch
    end
    acs.ampCorr = [];
    acs = rmfield(acs, 'ampCorr');
    % and save
    save(ampCorrFile, '-struct', 'acs');
    updateAxes(handles);
end
