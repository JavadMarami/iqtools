function varargout = iqcorrection(iqdata, fs, fpoints, normalize)
% perform amplitude flatness compensation on a complex signal using the
% frequency response that has been captured previously with iqtone 
%
% usage: result = corrected waveform
%   iqdata - input waveform. If iqdata = [], then the complex correction is returned.
%   fs - sample rate in Hz
%   fpoints - number of frequency points for compensation (optional)
%   normalize - if set to 1, scale to -1...+1 after applying correction (default: 0)
%
% Thomas Dippon, Agilent Technologies 2011-2013, Keysight Technologies 2014
%
% Disclaimer of Warranties: THIS SOFTWARE HAS NOT COMPLETED AGILENT'S FULL
% QUALITY ASSURANCE PROGRAM AND MAY HAVE ERRORS OR DEFECTS. AGILENT MAKES 
% NO EXPRESS OR IMPLIED WARRANTY OF ANY KIND WITH RESPECT TO THE SOFTWARE,
% AND SPECIFICALLY DISCLAIMS THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
% FITNESS FOR A PARTICULAR PURPOSE.
% THIS SOFTWARE MAY ONLY BE USED IN CONJUNCTION WITH AGILENT INSTRUMENTS. 

    if (~exist('normalize', 'var') || isempty(normalize))
        normalize = 1;
    end
    [ampCorr perChannelCorr] = readCorr();
    % if input waveform is empty, simply return the correction vector (e.g.
    % for display purposes)
    if (isempty(iqdata))
        varargout{1} = ampCorr;
        varargout{2} = perChannelCorr;
        return;
    end
    varargout{1} = iqdata;
    if (isempty(ampCorr))
        return;
    end
    % make sure the data is in the correct format
    if (isvector(iqdata) && size(iqdata,1) > 1)
        iqdata = iqdata.';
    end
    % if column 3 exists, it contains the complex correction
    if (size(ampCorr,2) >= 3)
        iqresult = applyFFTcorr(iqdata, fs, ampCorr(:,1), ampCorr(:,3));
    else
        iqresult = applyCONVcorr(iqdata, fs, ampCorr(:,1), ampCorr(:,2));
    end
    % if per-channel correction exists, apply it as well
    if (~isempty(perChannelCorr))
        ires = real(applyFFTcorr(real(iqresult), fs, perChannelCorr(:,1), perChannelCorr(:,2)));
        qres = real(applyFFTcorr(imag(iqresult), fs, perChannelCorr(:,1), perChannelCorr(:,3)));
        iqresult = complex(ires, qres);
    end
    % normalize
    if (normalize)
        scale = max(max(abs(real(iqresult))), max(abs(imag(iqresult))));
        iqresult = iqresult ./ scale;
    end
    varargout{1} = iqresult;
end


function iqresult = applyFFTcorr(iqdata, fs, freq, cplxCorr)
    % if we don't have negative frequencies, mirror them
    if (min(freq) >= 0)
        if (freq(1) == 0)            % don't duplicate zero-frequency
            startIdx = 2;
        else
            startIdx = 1;
        end
        freq = [-1 * flipud(freq); freq(startIdx:end)];
        cplxCorr = [conj(flipud(cplxCorr)); cplxCorr(startIdx:end,:)]; % negative side must use complex conjugate
    end
    fdata = fftshift(fft(iqdata));
    points = length(fdata);
    newFreq = linspace(-0.5, 0.5-1/points, points) * fs;
    % for operation in second Nyquist band, shift the X axis
%    if (sum(ampCorr(:,1)) / length(ampCorr(:,1)) > fs/2)
%        newFreq = newFreq + fs;
%    end
    % interpolate the correction curve to match the data
    corrLin = interp1(freq, cplxCorr, newFreq, 'pchip', 1);
    % apply the correction and convert back to time domain
    iqresult = ifft(fftshift(fdata .* corrLin));
end


function iqresult = applyCONVcorr(iqdata, fs, freq, mag)
    freq = ampCorr(:,1);
    mag = ampCorr(:,2);
    % if we don't have negative frequencies, mirror them
    if (min(freq) >= 0)
        if (freq(1) == 0)            % don't duplicate zero-frequency
            startIdx = 2;
        else
            startIdx = 1;
        end
        freq = [-1 * flipud(freq); freq(startIdx:end)];
        mag = [flipud(mag); mag(startIdx:end)];
    end
    % amplitude correction only --> convert into FIR filter
    if (~exist('fpoints') || isempty(fpoints))
        fpoints = 128;
        ldspacing = ceil(log2(fs/fpoints / min(diff(freq))));
        % make sure the frequency interval is at least as "fine" as in the
        % amplitude correction file
        if (ldspacing > 0)
            fpoints = fpoints * 2^ldspacing;
        end
    end
    % if the frequency values are above 1st Nyquist, move them down
    if (min(freq) >= fs && max(freq) <= 3*fs/2)     % 3rd Nyquist
        freq = freq - fs;
    end
    if (min(freq) >= fs/2 && max(freq) <= fs)       % 2nd Nyquist
        freq = fs - freq;
        freq = flipud(freq);
        mag = flipud(mag);
    end
    % if the amplitude correction files consists of only positive
    % frequencies, assume the same frequency response for negative side
    if (min(freq) > 0)
        freq = [-1 * flipud(freq); freq];
        mag = [flipud(mag); mag];
    end
    % extend the freqeuncy span to +/- fs/2
    f1 = freq(1) - (0.1 * (freq(1) + fs/2));
    f2 = freq(end) + (0.1 * (fs/2 - freq(end)));
    if (f1 > -fs/2)
        freq = [-fs/2;     f1; freq;];
        mag  = [ -100; mag(1);  mag;];
    end
    if (f2 < fs/2)
        freq = [freq;       f2; fs/2];
        mag  = [mag;  mag(end); -100];
    end
    % create a vector of equally spaced frequencies and associated magnitudes
    newfreq = linspace(-fs/2, fs/2 - fs/fpoints, fpoints);
    newmag = interp1(freq, mag, newfreq, 'pchip');
    linmag = 10 .^ (newmag ./ 20);
    %... and derive a filter
    ampFilt = fftshift(ifft(fftshift(linmag)));
    % apply the filter to the signal with wrap-around to assure phase
    % continuity
    len = length(iqdata);
    nfilt = length(ampFilt);
    wrappedIQ = [iqdata(end-mod(nfilt,len)+1:end) repmat(iqdata, 1, floor(nfilt/len)+1)];
    tmp = filter(ampFilt, 1, wrappedIQ);
    iqresult = tmp(nfilt+1:end);
end

% read the frequency/phase correction from the "ampCorr.mat" file
% returns an array with three columns: freq, amplitude in dB, complex
% correction
function [corr perChannelCorr] = readCorr()
corr = [];
perChannelCorr = [];
acs = [];
try
    acs = load(iqampCorrFilename());
catch
end
try
    if (isempty(acs) || ~isfield(acs, 'ampCorr'))   % no correction file at all
        acs.ampCorr = [1e6 0 1; 2e6 0 1];
    end
    cutoff = 0; % zero means no cutoff frequency
    if (isfield(acs, 'sparamCutoff'))
        cutoff = acs.sparamCutoff;
    end
    ampCorr = acs.ampCorr;
    if (isfield(acs, 'ampCorrMode'))
        ampCorrMode = acs.ampCorrMode;
    else
        ampCorrMode = -1;   % old style: de-embed
    end
    if (size(ampCorr,2) <= 2)  % no complex correction available
        ampCorr(:,3) = 10.^(ampCorr(:,2)/20);
    end
    if (ampCorrMode == 1)
        ampCorr(:,3) = conj(ampCorr(:,3));
        ampCorr(:,2) = -1 * ampCorr(:,2);
    elseif (ampCorrMode == 0)
        ampCorr(:,3) = ones(size(ampCorr, 1), 1);
        ampCorr(:,2) = zeros(size(ampCorr, 1), 1);
    end
    % check is we have perChannel corrections
    if (isfield(acs, 'perChannelCorr'))
        perChannelCorr = acs.perChannelCorr;
    end
    % check if we use an S-Parameter file
    if (isfield(acs, 'sparamFile'))
        [path, name, ext] = fileparts(acs.sparamFile);
        if (isfield(acs, 'sparamMode') && acs.sparamMode ~= 0) % use it?
            amp2 = readSParam(acs.sparamFile, acs.sparamMode, acs.selectedSParam, 0);
            if (~isempty(amp2))
                if (isempty(perChannelCorr))
                    % if this is the only one, we're done
                    perChannelCorr = amp2;
                else
                    % use both methods - now it gets complicated...
                    clear amp2New;
                    amp2New(:,1) = union(perChannelCorr(:,1), amp2(:,1));
                    amp2New(:,2) = interp1(amp2(:,1), amp2(:,2), amp2New(:,1), 'pchip', sum(amp2(:,2)/size(amp2,1)));
                    amp2New(:,2:size(perChannelCorr,2)) = repmat(amp2New(:,2), 1, size(perChannelCorr,2)-1);
                    clear ampCorrNew;
                    ampCorrNew(:,1) = amp2New(:,1);
                    mappedPerCh = interp1(perChannelCorr(:,1), perChannelCorr(:,2:end), ampCorrNew(:,1), 'pchip', sum(perChannelCorr(:,2)/size(perChannelCorr,1)));
                    ampCorrNew(:,2:size(perChannelCorr,2)) = amp2New(:,2:end) .* mappedPerCh;
                    perChannelCorr = ampCorrNew;
                end
            end
        end
    end
    
    % apply cutoff frequency
    if (cutoff ~= 0)
        if (~isempty(ampCorr))
            idx = abs(ampCorr(:,1)) > cutoff;
            ampCorr(idx,:) = [];
        end
        if (~isempty(perChannelCorr))
            idx = abs(perChannelCorr(:,1)) > cutoff;
            perChannelCorr(idx,:) = [];
        end
    end
    corr = ampCorr;

    catch ex
        errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)], 'Did you ever run a correction?'});
    end
end


% read an S-parameter file (s2p, s4p, s6p) and convert the selected
% S-Parameter into the "internal" format
% sparamFile - full pathname of S-parameter file
% sparamMode - 0=don't use, 1=embed, -1=de-embed
% selectedSParam - vector with 2 elements, e.g. [2 1] --> S21
% mirror - mirror to negative frequencies
% returns 3-column array with columns = frequency, mag(dB), complex corr
function ampCorr = readSParam(sparamFile, sparamMode, selectedSParam, mirror)
ampCorr = [];
try
    %sp = reads4p(sparamFile);
    sp = read(rfdata.data, sparamFile);
catch ex
    errordlg({'Error reading ' sparamFile ' ' ex.message});
    return;
end
freq = sp.Freq;
selS = squeeze(sp.S_Parameters(selectedSParam(1), selectedSParam(2), :));
if (mirror)
    % assume the same behaviour for positive & negative frequencies
    if (freq(1) == 0)            % don't duplicate zero-frequency
        startIdx = 2;
    else
        startIdx = 1;
    end
    freq = [-1 * flipud(freq); freq(startIdx:end)];
    selS = [conj(flipud(selS)); selS(startIdx:end)]; % negative side must use complex conjugate
end
%mag = 20*log10(abs(selS));
%phi = unwrap(angle(selS));

clear ampCorr;
ampCorr(:,1) = freq;
switch sparamMode
    case 1
        ampCorr(:,2) = selS;
    case -1
        ampCorr(:,2) = 1 ./ selS;
    case 0
        ampCorr(:,2) = ones(size(ampCorr,1), 1);
    otherwise
        error('unexpected type');
end
end
