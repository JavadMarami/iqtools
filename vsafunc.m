function result = vsafunc(vsaApp, fct, varargin)
% Send commands and retrieve measurements from the 89600 VSA software.
% This function is not intended to be called directly from the command line, but from
% higher-level scripts (e.g. iqmod_gui.m)
%
% It uses the .NET interface to communicate with the VSA software. If a running instance
% can be found, it will be used. Otherwise a new instance of VSA is launched.
%
% Thomas Dippon, Agilent Technologies 2011-2013, Keysight Technologies 2014
%
% Disclaimer of Warranties: THIS SOFTWARE HAS NOT COMPLETED AGILENT'S FULL
% QUALITY ASSURANCE PROGRAM AND MAY HAVE ERRORS OR DEFECTS. AGILENT MAKES 
% NO EXPRESS OR IMPLIED WARRANTY OF ANY KIND WITH RESPECT TO THE SOFTWARE,
% AND SPECIFICALLY DISCLAIMS THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
% FITNESS FOR A PARTICULAR PURPOSE.
% THIS SOFTWARE MAY ONLY BE USED IN CONJUNCTION WITH AGILENT INSTRUMENTS. 

result = [];
try
switch fct
    % attach to VSA session. If VSA is not running, start a new session
    % no additional arguments
    case 'open'
        cfg = loadArbConfig();
        if (isfield(cfg, 'visaAddrVSA'))    % use SCPI
            vsaApp = iqopen(cfg.visaAddrVSA);
            if (isempty(vsaApp))
                errordlg({'Can not open connection to VSA software at ' cfg.visaAddrVSA ...
                    'Please check VSA address in configuration window, ' ...
                    'launch VSA and make sure it is configured for SCPI access'});
            end
        else
            mypath = [fileparts(which('vsafunc')) '\Interfaces\'];
            pwdpath = [pwd '\Interfaces\'];
            pathlist = { ...
                mypath ...
                pwdpath ...
                'C:\Program Files (x86)\Agilent\89600B VSA 14.0\89601B VSA\Interfaces_v3.5\' ...
                'C:\Program Files (x86)\Agilent\89600B VSA 14.2\89601B VSA\Interfaces_v3.5\' ...
                'C:\Program Files (x86)\Agilent\89600B VSA 15.0\89601B VSA\Interfaces_v3.5\' ...
                'C:\Program Files\Agilent\89600 Software 16.0\89600 VSA Software\Interfaces_v3.5\' ...
                 };
            asm1Name = 'Agilent.SA.Vsa.Interfaces.dll';
            asm2Name = 'Agilent.SA.Vsa.DigitalDemod.Interfaces.dll';
            asm3Name = 'Agilent.SA.Vsa.CustomOfdm.Interfaces.dll';
            found = 0;
            for i = 1:length(pathlist)
                try
                    NET.addAssembly(strcat(pathlist{i}, asm1Name));
                    NET.addAssembly(strcat(pathlist{i}, asm2Name));
                    NET.addAssembly(strcat(pathlist{i}, asm3Name));
                    found = 1;
                    break;  % if execution is at this point, we found the DLLs
                catch e
                end
            end
            if (~found)
                errordlg([{'Can''t find VSA Interface DLLs in any of the following directories:' ' '} pathlist]);
                return;
            end
            import Agilent.SA.Vsa.*;
            % Attach to a running instance of VSA. If there no running instance, 
            % create one.
            vsaApp = ApplicationFactory.Create();
            if (isempty(vsaApp))
                hMsgBox = msgbox({'Starting VSA software...', 'Please wait, this can take up to 1 minute'});
                try
                    vsaApp = ApplicationFactory.Create(true, '', '', -1);
                catch e
                end
                close(hMsgBox);
            end
            if (isempty(vsaApp))
                errordlg('Can''t open VSA Software');
                return;
            end
            % Make VSA visible
            vsaApp.IsVisible = true;
        end
        result = vsaApp;
        assignin('base', 'vsaApp', vsaApp);
%-------------------------------------------------------------------------        
    case 'preset'
        % set VSA default settings
        % no additional arguments
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaDisp = vsaApp.Display;
            % Preset to defaults
% don't reset from scratch every time, since it takes a long time and
% switches relays unnecessarily in the scope & spectrum analyzer
%        vsaDisp.Preset;
%        vsaMeas.Preset;
            vsaMeas.Reset;
            try
                vsaMeas.WaitForMeasurementDone(5000);
                vsaMeas.Input.Analog.AutoRange;
                vsaMeas.WaitForMeasurementDone(5000);
            catch
            end
            vsaMeas.Pause();
        elseif (isa(vsaApp, 'tcpip'))
            xfprintf(vsaApp, ':SYST:PRES:MEAS');
            xfprintf(vsaApp, ':INIT:PAUS');
        end
%-------------------------------------------------------------------------        
    case 'autorange'
        % if an argument is given, then set the range to this value
        % otherwise: autorange
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            try
                if (length(varargin) >= 1)
                    vsaAnalog = vsaMeas.Input.Analog;
                    vsaAnalog.Range = varargin{1};
                else
                    vsaMeas.WaitForMeasurementDone(5000);
                    vsaMeas.Input.Analog.AutoRange;
                    vsaMeas.WaitForMeasurementDone(5000);
                end
            catch
            end
        elseif (isa(vsaApp, 'tcpip'))
            if (length(varargin) >= 1)
                xfprintf(vsaApp, sprintf(':INP:ANAL:RANG %g', varargin{1}));
            else
                pause(1);
                xfprintf(vsaApp, sprintf(':INP:ANAL:RANG:AUTO'));
                query(vsaApp, '*OPC?');
            end
        end
%-------------------------------------------------------------------------        
    case 'load'
        % load a vector
        % argument 1: data vector
        % argument 2: sampleRate
        Y = reshape(varargin{1}, numel(varargin{1}), 1);
        % create a certain minimum number of samples so that VSA does not
        % complain about a record length being too short
        minLength = 100000;
        if (length(Y) < minLength)
            Y = repmat(Y, ceil(minLength/length(Y)), 1);
        end
        % add a tiny bit of noise to make the VSA display look realistic ;-)
        Y = awgn(Y,300);
        XStart = 0;
        XDelta = 1/varargin{2};
        InputZoom = 1;
        file = [pwd '\vsatemp.mat'];
        save(file, 'Y', 'XStart', 'XDelta', 'InputZoom');
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaMeas.Input.Recording.RecallFile(file, 'MAT');
            vsaInput = vsaMeas.Input;
            vsaInput.DataFrom = DataSource.Recording;
    %        vsafunc(vsaApp, 'input', 1);    % set VSA to baseband
        elseif (isa(vsaApp, 'tcpip'))
            xfprintf(vsaApp, sprintf(':MMEMory:LOAD:RECording "%s","MAT"', file));
            xfprintf(vsaApp, sprintf(':INPUT:DATA REC'));
        end
%-------------------------------------------------------------------------        
    case 'fromHW'
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaInput = vsaMeas.Input;
            vsaInput.DataFrom = DataSource.Hardware;
        elseif (isa(vsaApp, 'tcpip'))
            xfprintf(vsaApp, sprintf(':INPUT:DATA HW'));
        end
%-------------------------------------------------------------------------        
    case 'input'
        % argument 1: center frequency (0 -> IQ, not zero 0 -> baseband)
        % argument 2: channel configuration string
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaInput = vsaMeas.Input;
            types = vsaInput.LogicalChannelTypesSupported;
            if (types.Count > 1)
                logicalChTypes = NET.createArray('Agilent.SA.Vsa.LogicalChannelType', 1);
                if (varargin{1} == 0)
                    logicalChTypes(1) = Agilent.SA.Vsa.LogicalChannelType.IQ;
                else
                    logicalChTypes(1) = Agilent.SA.Vsa.LogicalChannelType.Baseband;
                end
                try
                    vsaInput.ChangeLogicalChannels(logicalChTypes);
                catch ex
                    errordlg('Can not set the input channels. Please connect VSA to the appropriate analyzer hardware, then try again.','Error')  
                end
            end
        elseif (isa(vsaApp, 'tcpip'))
            if (length(varargin) >= 2)
                ch = varargin{2};
                if (varargin{1} == 0)
                    xfprintf(vsaApp, sprintf(':INPut:CHANnel:CONFigure:CUSTom "IQ_%d_%d"', ch(1), ch(2)));
                else
                    xfprintf(vsaApp, sprintf(':INPut:CHANnel:CONFigure:CUSTom "%d"', ch(1)));
                end
            else
                if (varargin{1} == 0)
                    xfprintf(vsaApp, sprintf(':INPut:CHANnel:CONFigure IQ'));
                else
                    xfprintf(vsaApp, sprintf(':INPut:CHANnel:CONFigure BB'));
                end
            end
        end
%-------------------------------------------------------------------------        
    case 'start'
        % start measurement
        % argument 1: continuous(1) or single(0) (optional)
        if (length(varargin) >= 1 && varargin{1} ~= 0)
            cont = true;
        else
            cont = false;
        end
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaMeas.IsContinuous = cont;
            vsaMeas.Restart();
            try
                vsaMeas.WaitForMeasurementDone(5000);
            catch
            end
        elseif (isa(vsaApp, 'tcpip'))
            xfprintf(vsaApp, sprintf(':INIT:CONT %d', cont));
            xfprintf(vsaApp, sprintf(':INIT:RESTART'));
        end
%-------------------------------------------------------------------------        
    case 'freq'
        % set center and span
        % argument 1: center frequency
        % argument 2: span (optional)
        % argument 3: points (optional)
        % argument 4: windowType (optional)
        % argument 5: average (optional)
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaFreq = vsaMeas.Frequency;
            vsaFreq.Center = varargin{1};
            if (length(varargin) >= 2)
                vsaFreq.Span = varargin{2};
            end
            if (length(varargin) >= 3)
                vsaFreq.Points = varargin{3};
            end
            if (length(varargin) >= 4)
                switch (varargin{4})
                    case 'flattop'
                        vsaFreq.Window = Agilent.SA.Vsa.WindowType.FlatTop;
                    case 'uniform'
                        vsaFreq.Window = Agilent.SA.Vsa.WindowType.Uniform;
                    otherwise
                        errordlg(sprintf('invalid window: %s', varargin{4}));
                end
            end
            if (length(varargin) >= 5)
                average = vsaMeas.Average;
                if (varargin{5} > 0)
                    average.Style = Agilent.SA.Vsa.AverageStyle.RmsExponential;
                    average.Count = varargin{5};
                    average.IsFast = false;
                    average.IsRepeat = false;
                else
                    average.Style = Agilent.SA.Vsa.AverageStyle.Off;
                end
            end
            if (length(varargin) >= 6)
                vsaTime = vsaMeas.Time;
                vsaTime.Length = varargin{6};
            end
        elseif (isa(vsaApp, 'tcpip'))
            xfprintf(vsaApp, sprintf(':FREQ:CENT %g', varargin{1}));
            if (length(varargin) >= 2)
                xfprintf(vsaApp, sprintf(':FREQ:SPAN %g', varargin{2}));
            end
            if (length(varargin) >= 3)
                xfprintf(vsaApp, sprintf(':RBW:POINTS %d', varargin{3}));
            end
            if (length(varargin) >= 4)
                xfprintf(vsaApp, sprintf(':RBW:WINDOW "%s"', varargin{4}));
            end
            if (length(varargin) >= 5)
                if (varargin{5} > 0)
                    xfprintf(vsaApp, sprintf(':AVER:STYLE "RmsExponential"'));
                    xfprintf(vsaApp, sprintf(':AVER:COUNT %d', varargin{5}));
                    xfprintf(vsaApp, sprintf(':AVER:FAST 0'));
                    xfprintf(vsaApp, sprintf(':AVER:REPEAT 0'));
                else
                    xfprintf(vsaApp, sprintf(':AVER:STYLE "Off"'));
                end
            end
            if (length(varargin) >= 6)
                xfprintf(vsaApp, sprintf(':TIME:LENGTH %g', varargin{6}));
            end
        end
%-------------------------------------------------------------------------        
    case 'trace'
        % set layout and trace contents
        % argument 1: trace layout (2=2, 4=2x2, 6=2x3)
        % argument 2: 'DigDemod' or 'Chirp' (optional)
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaDisp = vsaApp.Display;
            vsaTraces = vsaDisp.Traces;
            switch varargin{1}
                case 1; 
                    vsaTraces.ArrangeWindows(Agilent.SA.Vsa.ArrangeWindowHints.HorizontalOrientation,1,1);
                case 2;
                    vsaTraces.ArrangeWindows(Agilent.SA.Vsa.ArrangeWindowHints.HorizontalOrientation,1,2);
                case 4;
                    vsaTraces.ArrangeWindows(Agilent.SA.Vsa.ArrangeWindowHints.FillAll,2,2);
                    vsaTraces.ArrangeWindows(Agilent.SA.Vsa.ArrangeWindowHints.HorizontalOrientation,2,2);
                case 6;
                    vsaTraces.ArrangeWindows(Agilent.SA.Vsa.ArrangeWindowHints.FillAll,2,3);
                    vsaTraces.ArrangeWindows(Agilent.SA.Vsa.ArrangeWindowHints.HorizontalOrientation,2,3);
            end
            for i = varargin{1}:(vsaTraces.Count - 1)
                tr = vsaTraces.Item(i);
                tr.IsVisible = false;
            end
            if (length(varargin) >= 2)
                switch varargin{2}
                    case 'Tones'
                        tr = vsaTraces.Item(0); tr.DataName = 'Spectrum1';
                    case 'DigDemod'
                        tr = vsaTraces.Item(0); tr.DataName = 'IQ Meas Time1';
                        tr = vsaTraces.Item(1); tr.DataName = 'Spectrum1';     tr.Format = Agilent.SA.Vsa.TraceFormatType.LogMagnitude;
                        %tr = vsaTraces.Item(2); tr.DataName = 'Error Vector Time1'; 
                        tr = vsaTraces.Item(2); tr.DataName = 'IQ Meas Time1'; tr.Format = Agilent.SA.Vsa.TraceFormatType.EyeI;
                        tr = vsaTraces.Item(3); tr.DataName = 'Syms/Errs1';
                        if (varargin{1} == 6)
                            tr = vsaTraces.Item(4); tr.DataName = 'Ch Frequency Response1';
                              tr.Format = Agilent.SA.Vsa.TraceFormatType.Real;
                            tr = vsaTraces.Item(5); tr.DataName = 'Ch Frequency Response1';
                              tr.Format = Agilent.SA.Vsa.TraceFormatType.Imaginary;
                        end
                    case 'CustomOFDM'
                        tr = vsaTraces.Item(0); tr.DataName = 'IQ Meas1';
                        tr = vsaTraces.Item(1); tr.DataName = 'Spectrum1'; tr.Format = Agilent.SA.Vsa.TraceFormatType.LogMagnitude;
                        tr = vsaTraces.Item(2); tr.DataName ='RMS Error Vector Spectrum1'; 
                        tr = vsaTraces.Item(3); tr.DataName = 'OFDM Error Summary1';
                        if (varargin{1} == 6)
                            tr = vsaTraces.Item(4); tr.DataName = 'Ch Frequency Response1';
                              tr.Format = Agilent.SA.Vsa.TraceFormatType.Real;
                            tr = vsaTraces.Item(5); tr.DataName = 'Ch Frequency Response1';
                              tr.Format = Agilent.SA.Vsa.TraceFormatType.Imaginary;
                        end
                    case 'Chirp'
                        tr = vsaTraces.Item(0); tr.DataName = 'Spectrum1';  tr.Format = Agilent.SA.Vsa.TraceFormatType.LogMagnitude;
                        tr = vsaTraces.Item(1); tr.DataName = 'Main Time1'; tr.Format = Agilent.SA.Vsa.TraceFormatType.LogMagnitude;
                        tr = vsaTraces.Item(2); tr.DataName = 'Main Time1'; tr.Format = Agilent.SA.Vsa.TraceFormatType.UnwrapPhase;
                        tr = vsaTraces.Item(3); tr.DataName = 'Main Time1'; tr.Format = Agilent.SA.Vsa.TraceFormatType.GroupDelay;
                end
            end
        elseif (isa(vsaApp, 'tcpip'))
            switch varargin{1}
                case 1; 
                    xfprintf(vsaApp, ':DISP:LAYOUT 1,1');
                case 2;
                    xfprintf(vsaApp, ':DISP:LAYOUT 2,1');
                case 4;
                    xfprintf(vsaApp, ':DISP:LAYOUT 2,2');
                case 6;
                    xfprintf(vsaApp, ':DISP:LAYOUT 2,3');
            end
            numTr = str2double(query(vsaApp, ':TRAC:COUNT?'));
            for i = (varargin{1} + 1):numTr
                xfprintf(vsaApp, sprintf(':TRAC%d:VISIBLE 0', i));
            end
            if (length(varargin) >= 2)
                switch varargin{2}
                    case 'Tones'
                        xfprintf(vsaApp, sprintf(':TRAC1:DATA:NAME "%s"; FORMAT "%s"', 'Spectrum1', 'LogMagnitude'));
                    case 'DigDemod'
                        xfprintf(vsaApp, sprintf(':TRAC1:DATA:NAME "%s"; FORMAT "%s"', 'IQ Meas Time1', 'Real'));
                        xfprintf(vsaApp, sprintf(':TRAC2:DATA:NAME "%s"; FORMAT "%s"', 'Spectrum1', 'LogMagnitude'));
                        xfprintf(vsaApp, sprintf(':TRAC3:DATA:NAME "%s"; FORMAT "%s"', 'IQ Meas Time1', 'EyeI'));
                        xfprintf(vsaApp, sprintf(':TRAC4:DATA:NAME "%s"', 'Syms/Errs1'));
                        if (varargin{1} == 6)
                            xfprintf(vsaApp, sprintf(':TRAC5:DATA:NAME "%s"; FORMAT "%s"', 'Ch Frequency Response1', 'Real'));
                            xfprintf(vsaApp, sprintf(':TRAC6:DATA:NAME "%s"; FORMAT "%s"', 'Ch Frequency Response1', 'Imaginary'));
                        end
                    case 'CustomOFDM'
                        xfprintf(vsaApp, sprintf(':TRAC1:DATA:NAME "%s"', 'IQ Meas1'));
                        xfprintf(vsaApp, sprintf(':TRAC2:DATA:NAME "%s"; :TRAC2:DATA:FORMAT "%s"', 'Spectrum1', 'LogMagnitude'));
                        xfprintf(vsaApp, sprintf(':TRAC3:DATA:NAME "%s"', 'RMS Error Vector Spectrum1'));
                        xfprintf(vsaApp, sprintf(':TRAC4:DATA:NAME "%s"', 'OFDM Error Summary1'));
                        if (varargin{1} == 6)
                            xfprintf(vsaApp, sprintf(':TRAC5:DATA:NAME "%s"; :TRAC5:DATA:FORMAT "%s"', 'Ch Frequency Response1', 'Real'));
                            xfprintf(vsaApp, sprintf(':TRAC6:DATA:NAME "%s"; :TRAC6:DATA:FORMAT "%s"', 'Ch Frequency Response1', 'Imaginary'));
                        end
                    case 'Chirp'
                        xfprintf(vsaApp, sprintf(':TRAC1:DATA:NAME "%s"; :TRAC1:DATA:FORMAT "%s"', 'Spectrum1', 'LogMagnitude'));
                        xfprintf(vsaApp, sprintf(':TRAC2:DATA:NAME "%s"; :TRAC2:DATA:FORMAT "%s"', 'Main Time1', 'LogMagnitude'));
                        xfprintf(vsaApp, sprintf(':TRAC3:DATA:NAME "%s"; :TRAC3:DATA:FORMAT "%s"', 'Main Time1', 'UnwrapPhase'));
                        xfprintf(vsaApp, sprintf(':TRAC4:DATA:NAME "%s"; :TRAC4:DATA:FORMAT "%s"', 'Main Time1', 'GroupDelay'));
                end
            end
        end
%-------------------------------------------------------------------------        
    case 'autoscale'
        % autoscale the selected traces
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaDisp = vsaApp.Display;
            vsaTraces = vsaDisp.Traces;
            if (length(varargin) < 1)
                items = 1:vsaTraces.Count;
            else
                for i=1:length(varargin)
                    items(i) = varargin{i};
                end
            end
            for i=1:length(items)
                tr = vsaTraces.Item(items(i)-1);
                tr.YScaleAuto();
            end
        elseif (isa(vsaApp, 'tcpip'))
            xfprintf(vsaApp, sprintf(':INPUT:RANGE AUTO'));
        end
%-------------------------------------------------------------------------        
    case 'DigDemod'
        % set Digital Demod parameters
        % argument 1: modulation type
        % argument 2: symbol rate
        % argument 3: filter type (optional)
        % argument 4: filter alpha/BT (optional)
        % argument 5: result length (optional)

        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaDemod = getDigDemod(vsaApp);
            %Set the VSA measurement parameters
            switch varargin{1}
                case 'BPSK';   format = Agilent.SA.Vsa.DigitalDemod.Format.Bpsk;
                case 'BPSK_X'; format = Agilent.SA.Vsa.DigitalDemod.Format.Bpsk;
                case 'QPSK';   format = Agilent.SA.Vsa.DigitalDemod.Format.Qpsk;
                case 'OQPSK';  format = Agilent.SA.Vsa.DigitalDemod.Format.OffsetQpsk;
                case '8-PSK';  format = Agilent.SA.Vsa.DigitalDemod.Format.Psk8;
                case 'QAM4';   format = Agilent.SA.Vsa.DigitalDemod.Format.Qpsk;
                case 'QAM16';  format = Agilent.SA.Vsa.DigitalDemod.Format.Qam16;
                case 'QAM32';  format = Agilent.SA.Vsa.DigitalDemod.Format.Qam32;
                case 'QAM64';  format = Agilent.SA.Vsa.DigitalDemod.Format.Qam64;
                case 'QAM128'; format = Agilent.SA.Vsa.DigitalDemod.Format.Qam128;
                case 'QAM256'; format = Agilent.SA.Vsa.DigitalDemod.Format.Qam256;
                case 'QAM512'; format = Agilent.SA.Vsa.DigitalDemod.Format.Qam512;
                case 'QAM1024'; format = Agilent.SA.Vsa.DigitalDemod.Format.Qam1024;
                case 'APSK16'; format = Agilent.SA.Vsa.DigitalDemod.Format.Apsk16;
                    vsaDemod.ApskRing2Ratio = 2.6;
                case 'APSK32'; format = Agilent.SA.Vsa.DigitalDemod.Format.Apsk32;
                    vsaDemod.ApskRing2Ratio = 2.84;
                    vsaDemod.ApskRing3Ratio = 5.27;
                case 'PAM4'; format = Agilent.SA.Vsa.DigitalDemod.Format.CustomApsk;
                case 'CPM'; format = Agilent.SA.Vsa.DigitalDemod.Format.CpmFM;
                    vsaDemod.CpmIndex = Agilent.SA.Vsa.DigitalDemod.CpmIndex.MinimumShiftKeying;
                otherwise; errordlg(['unknown format: ' varargin{1}]);
            end
            try
                vsaDemod.Format = format;
                vsaDemod.SymbolRate = varargin{2};
                vsaDemod.IsFrequencyMirrored = false;
                vsaDemod.IsPulseSearchEnabled = false;
            catch
                msgbox('Error controlling VSA - please try again');
            end
            if (length(varargin) >= 3)
                switch varargin{3}
                    case 'Square Root Raised Cosine';
                        mfilter = Agilent.SA.Vsa.DigitalDemod.MeasurementFilter.RootRaisedCosine;
                        rfilter = Agilent.SA.Vsa.DigitalDemod.ReferenceFilter.RaisedCosine;
                    case 'Raised Cosine';
                        mfilter = Agilent.SA.Vsa.DigitalDemod.MeasurementFilter.None;
                        rfilter = Agilent.SA.Vsa.DigitalDemod.ReferenceFilter.RaisedCosine;
                    case 'Gaussian';
                        mfilter = Agilent.SA.Vsa.DigitalDemod.MeasurementFilter.Gaussian;
                        rfilter = Agilent.SA.Vsa.DigitalDemod.ReferenceFilter.Gaussian;
                    case 'Rectangular';
                        mfilter = Agilent.SA.Vsa.DigitalDemod.MeasurementFilter.None;
                        rfilter = Agilent.SA.Vsa.DigitalDemod.ReferenceFilter.Rectangular;
                    case 'None';
                        mfilter = Agilent.SA.Vsa.DigitalDemod.MeasurementFilter.None;
                        rfilter = Agilent.SA.Vsa.DigitalDemod.ReferenceFilter.RectangularOneSymbolDuration;
                end
                % CPM seems to work best with 3RC filter
                if (format == Agilent.SA.Vsa.DigitalDemod.Format.CpmFM)
                    rfilter = Agilent.SA.Vsa.DigitalDemod.ReferenceFilter.RaisedCosineThreeSymbolDuration;
                end
                vsaDemod.MeasurementFilter = mfilter;
                vsaDemod.ReferenceFilter = rfilter;
            end
            vsaDemod.PointsPerSymbol = 5;
            if (length(varargin) >= 4)
                vsaDemod.FilterAlphaBT = varargin{4};
            end
            if (length(varargin) >= 5)
                vsaDemod.ResultLength = varargin{5};
            end
            if (strcmp(varargin{1}, 'PAM4'))
                uiwait(msgbox('Please set up VSA to demodulate PAM4 signal using custom APSK', 'Manual config', 'modal'));
            end
        elseif (isa(vsaApp, 'tcpip'))
            % ...still need to be implemented...
            xfprintf(vsaApp, sprintf(':BLA BLA BLA'));
        end
%-------------------------------------------------------------------------        
    case 'trigger'
        % set Trigger
        % argument 1: trigger mode ('FreeRun' or 'Channel')
        % argument 2: Level
        % argument 3: Delay
        % argument 4: Holdoff
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaMeas = vsaApp.Measurements.SelectedItem;
            vsaInput = vsaMeas.Input;
            vsaTrigger = vsaInput.Recording.Trigger;
            switch varargin{1}
                case 'FreeRun'
                    vsaTrigger.Style = Agilent.SA.Vsa.TriggerStyle.Auto;
                case 'Channel'
                    vsaTrigger.Style = Agilent.SA.Vsa.TriggerStyle.Level;
            end
            vsaTrigger.Level = varargin{2};
            vsaTrigger.Delay = varargin{3};
            vsaTrigger.Holdoff = varargin{4};
            vsaTrigger = vsaInput.Trigger;
            switch varargin{1}
                case 'FreeRun'
                    vsaTrigger.Style = Agilent.SA.Vsa.TriggerStyle.Auto;
                case 'Channel'
                    vsaTrigger.Style = Agilent.SA.Vsa.TriggerStyle.Level;
            end
            vsaTrigger.Level = varargin{2};
            vsaTrigger.Delay = varargin{3};
            vsaTrigger.Holdoff = varargin{4};
        elseif (isa(vsaApp, 'tcpip'))
            % ...still need to be implemented...
            xfprintf(vsaApp, sprintf(':BLA BLA BLA'));
        end
%-------------------------------------------------------------------------        
    case 'equalizer'
        % set up equalizer
        % argument 1: on/off (1/0)
        % argument 2: filterLength (optional)
        % argument 3: convergence (optional)
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaDemod = getDigDemod(vsaApp);
            try
                vsaDemod.IsEqualized = varargin{1};
                if (length(varargin) >= 2)
                    vsaDemod.EqualizerFilterLength = varargin{2};
                end
                if (length(varargin) >= 3)
                    vsaDemod.EqualizerConvergence = varargin{3};
                end
            catch
                msgbox('Error controlling VSA - please try again');
            end
        elseif (isa(vsaApp, 'tcpip'))
            % ...still need to be implemented...
            xfprintf(vsaApp, sprintf(':BLA BLA BLA'));
        end
%-------------------------------------------------------------------------        
    case 'readEqData'
        % get equalizer data and write ampCorr file
        % argument 1: add new measurement to existing correction (re-calibration) (Optional)
        % argument 2: carrierOffset to be subtracted from measurements (Optional)
        % argument 3: interleaving
        result = -1;
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaDisp = vsaApp.Display;
            vsaTraces = vsaDisp.Traces;
            tr = vsaTraces.Item(4);
            x = (tr.DoubleData(TraceDataSelect.X, false).double)';
            if (length(varargin) >= 2)
                x = x - varargin{2};
            end
            yreal = (tr.DoubleData(TraceDataSelect.Y, false).double)';
            tr = vsaTraces.Item(5);
            yimag = (tr.DoubleData(TraceDataSelect.Y, false).double)';
            y = 1 ./ complex(yreal, yimag);
            ydB = 20 * log10(abs(y));
        elseif (isa(vsaApp, 'tcpip'))
            % ...still need to be implemented...
            xfprintf(vsaApp, sprintf(':BLA BLA BLA'));
        end
        
        % combine with existing calibration data
        clear acs;
        acs.ampCorr = [x zeros(length(x),1) ones(length(x),1)];
        if (length(varargin) >= 1 && varargin{1} ~= 0)
            % in case of re-calibration, read the old cal values
            try
                % load the full correction file
                acs = load(iqampCorrFilename());
            catch
                clear acs;
            end
            if (isfield(acs, 'ampCorr'))
                % take care of ampCorrMode - flip or reset ampCorr
                % accordingly
                if (isfield(acs, 'ampCorrMode'))
                    ampCorrMode = acs.ampCorrMode;
                else
                    ampCorrMode = -1;   % old style: de-embed
                end
                if (size(acs.ampCorr,2) <= 2)  % no complex correction available
                    acs.ampCorr(:,3) = 10.^(acs.ampCorr(:,2)/20);
                end
                if (ampCorrMode == 1)
                    acs.ampCorr(:,3) = conj(acs.ampCorr(:,3));
                    acs.ampCorr(:,2) = -1 * acs.ampCorr(:,2);
                elseif (ampCorrMode == 0)
                    acs.ampCorr(:,3) = ones(size(acs.ampCorr, 1), 1);
                    acs.ampCorr(:,2) = zeros(size(acs.ampCorr, 1), 1);
                end
                % if the list of frequencies does not match,
                % interpolate the existing correction values
                if (~isequal(acs.ampCorr(:,1), x))
                    % but first check if new frequencies are positive & negative
                    % and old frequencies are only positive
                    if (min(acs.ampCorr(:,1)) >= 0 && min(x) < 0 && max(x) > 0)
                        freq = acs.ampCorr(:,1);
                        if (freq(1) == 0)            % don't duplicate zero-frequency
                            startIdx = 2;
                        else
                            startIdx = 1;
                        end
                        freq = [-1 * flipud(freq); freq(startIdx:end)];
                        mag = [flipud(acs.ampCorr(:,2)); acs.ampCorr(startIdx:end,2)];
                        if (size(acs.ampCorr,2) >= 3)
                            lincorr = [flipud(conj(acs.ampCorr(:,3))); acs.ampCorr(startIdx:end,3)];
                        else
                            lincorr = 10.^(mag/20);
                        end
                        acs.ampCorr = [freq mag lincorr];
                    end
                    % interpolate the existing correction values
                    newCol2 = interp1(acs.ampCorr(:,1), acs.ampCorr(:,2), x, 'pchip', 0);
                    if (size(acs.ampCorr,2) >= 3)
                        newCol3 = interp1(acs.ampCorr(:,1), acs.ampCorr(:,3), x, 'pchip', 1);
                    else
                        newCol3 = 10.^(newCol2/20);
                    end
                    acs.ampCorr = [x newCol2 newCol3];
                end
            else
                acs.ampCorr = [x zeros(length(x),1) ones(length(x),1)];
            end
        end
        % add new corrections to existing ones
        % dB's add up, complex (linear) corrections are multiplied
        acs.ampCorr = [x (acs.ampCorr(:,2)+ydB) (acs.ampCorr(:,3).*y)];
        acs.ampCorrMode = -1;
        % and save
        save(iqampCorrFilename(), '-struct', 'acs');
        result = 0;
%-------------------------------------------------------------------------        
    case 'mtone'
        % measure magnitude of tones
        % argument 1: vector of tones
        % argument 2: fc
        % argument 3: recalibrate
        % argument 4: interleaving
        result = [];
        tones = varargin{1};
        fc = varargin{2};
        recal = varargin{3};
        interleaving = varargin{4};
        origTones = tones;
        if (isa(vsaApp, 'Agilent.SA.Vsa.Application'))
            vsaDisp = vsaApp.Display;
            vsaTraces = vsaDisp.Traces;
            tr = vsaTraces.Item(0);
            marker = tr.Markers.Item(0);
            marker.IsVisible = true;
            mag = zeros(length(tones),1);
            for i=1:length(tones)
                marker.XData = tones(i) + fc;
                mag(i) = marker.Y;
            end
            marker.IsVisible = false;
            mag = sum(mag)/length(mag) - mag;
            clear acs;
            acs.ampCorr = [origTones zeros(length(origTones),1) ones(length(origTones),1)];
            if (recal)
                % in case of re-calibration, read the old cal values
                try
                    % load the full correction file
                    acs = load(iqampCorrFilename());
                catch
                    clear acs;
                end
                if (isfield(acs, 'ampCorr'))
                    % take care of ampCorrMode - flip or reset ampCorr
                    % accordingly
                    if (isfield(acs, 'ampCorrMode'))
                        ampCorrMode = acs.ampCorrMode;
                    else
                        ampCorrMode = -1;   % old style: de-embed
                    end
                    if (size(acs.ampCorr,2) <= 2)  % no complex correction available
                        acs.ampCorr(:,3) = 10.^(acs.ampCorr(:,2)/20);
                    end
                    if (ampCorrMode == 1)
                        acs.ampCorr(:,3) = conj(acs.ampCorr(:,3));
                        acs.ampCorr(:,2) = -1 * acs.ampCorr(:,2);
                    elseif (ampCorrMode == 0)
                        acs.ampCorr(:,3) = ones(size(acs.ampCorr, 1), 1);
                        acs.ampCorr(:,2) = zeros(size(acs.ampCorr, 1), 1);
                    end
                    % if the list of frequencies does not match, exit
                    if (~isequal(acs.ampCorr(:,1), origTones))
                        errordlg('Frequency points must be identical for re-calibration. Please perform initial calibration first.');
                        return;
                    end
                    % interpolate the existing correction values
                    if (~isequal(acs.ampCorr(:,1), origTones))
                        % but first check if new frequencies are positive & negative
                        % and old frequencies are only positive
                        if (min(acs.ampCorr(:,1)) >= 0 && min(origTones) < 0 && max(origTones) > 0)
                            freq = acs.ampCorr(:,1);
                            if (freq(1) == 0)            % don't duplicate zero-frequency
                                startIdx = 2;
                            else
                                startIdx = 1;
                            end
                            freq = [-1 * flipud(freq); freq(startIdx:end)];
                            magn = [flipud(acs.ampCorr(:,2)); acs.ampCorr(startIdx:end,2)];
                            if (size(acs.ampCorr,2) >= 3)
                                lincorr = [flipud(conj(acs.ampCorr(:,3))); acs.ampCorr(startIdx:end,3)];
                            else
                                lincorr = 10.^(magn/20);
                            end
                            acs.ampCorr = [freq magn lincorr];
                        end
                        % interpolate the existing correction values
                        newCol2 = interp1(acs.ampCorr(:,1), acs.ampCorr(:,2), origTones, 'pchip', 0);
                        if (size(acs.ampCorr,2) >= 3)
                            newCol3 = interp1(acs.ampCorr(:,1), acs.ampCorr(:,3), origTones, 'pchip', 1);
                        else
                            newCol3 = 10.^(newCol2/20);
                        end
                        acs.ampCorr = [origTones newCol2 newCol3];
                    end
                else
                    acs.ampCorr = [origTones zeros(length(origTones),1) ones(length(origTones),1)];
                end
            end
            % add new corrections to existing ones
            % dB's add up, complex (linear) corrections are multiplied
            acs.ampCorr = [origTones (acs.ampCorr(:,2)+mag) (acs.ampCorr(:,3).*(10.^(mag/20)))];
            acs.ampCorrMode = -1;
            % and save
            save(iqampCorrFilename(), '-struct', 'acs');
            result = 0;
        elseif (isa(vsaApp, 'tcpip'))
            % ...still need to be implemented...
            xfprintf(vsaApp, sprintf(':BLA BLA BLA'));
        end
%-------------------------------------------------------------------------        
    otherwise
        error(['unknown vsafunc: ' fct]);
end
catch ex
    errordlg({ex.message, [ex.stack(1).name ', line ' num2str(ex.stack(1).line)]});end
end


function vsaDemod = getDigDemod(vsaApp)
        vsaMeas = vsaApp.Measurements.SelectedItem;
        %switch to VSA Measurement (here I have an Error)
        DigDemodType = Agilent.SA.Vsa.DigitalDemod.MeasurementExtension.ExtensionType;
        DigDemodMeasExt = vsaMeas.SetMeasurementExtension(DigDemodType);
        vsaDemod = Agilent.SA.Vsa.DigitalDemod.MeasurementExtension.CastToExtensionType(DigDemodMeasExt);
        % [Start workaround for Matlab defect]
        vsaDemod.delete;
        DigDemodMeasExt = vsaMeas.SetMeasurementExtension(DigDemodType);
        vsaDemod = Agilent.SA.Vsa.DigitalDemod.MeasurementExtension.CastToExtensionType(DigDemodMeasExt);
        % [End workaround for Matlab defect]
end


function retVal = xfprintf(f, s, ignoreError)
% Send the string s to the instrument object f
% and check the error status
% if ignoreError is set, the result of :syst:err is ignored
% returns 0 for success, -1 for errors

    retVal = 0;
% set debugScpi=1 in MATLAB workspace to log SCPI commands
    if (evalin('base', 'exist(''debugScpi'', ''var'')'))
        fprintf('cmd = %s\n', s);
    end
    fprintf(f, s);
    result = query(f, ':syst:err?');
    if (isempty(result))
        fclose(f);
        errordlg({'VSA did not respond to a :SYST:ERRor query.' ...
            'Please check that VSA is running and accepting SCPI commands'}, 'Error');
        retVal = -1;
        return;
    end
    if (~exist('ignoreError', 'var') || ignoreError == 0)
        while (~strncmp(result, '+0,No error', 11) && ~strncmp(result, '+0,"No error"', 13))
            errordlg({'VSA Software returns an error on command:' s 'Error Message:' result});
            result = query(f, ':syst:err?');
            retVal = -1;
        end
    end
end
