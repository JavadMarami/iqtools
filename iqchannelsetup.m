function str = iqchannelsetup(cmd, pb, arbConfig, type)
% helper function for channel mapping dialog
% cmd - one of 'setup', 'mkstring', 'arraystring' (see comments below)
% pb - pushbutton object
% arbConfig - the current arbConfig struct
% type - 'single' or 'IQ' (default: IQ)
if (~exist('type', 'var'))
    type = 'IQ';
end
switch (cmd)
    % set up the UserData and String fields of the pushbutton
    % according to the selected AWG model and operation mode
    case 'setup'
        ud = get(pb, 'UserData');
        if (isempty(ud))
            % channel mapping is not defined at all yet -> set default values
            if (~isempty(strfind(arbConfig.model, 'M8195A')) || ...
                isfield(arbConfig, 'visaAddr2'))
                ud = [1 0; 1 0; 0 1; 0 1];
            else
                ud = [1 0; 0 1];
            end
        else
            % channel mapping is already defined
            if (~isempty(strfind(arbConfig.model, 'M8195A')) || ...
                 isfield(arbConfig, 'visaAddr2'))
                if (length(ud) < 4)
                    ud(3:4,:) = [1 0; 0 1];
                end
            else
                ud(3:end,:) = [];
            end
        end
        % don't do pulses on all channels...
        if (strcmp(type, 'pulse') && length(ud) == 4)
            ud = [1 0; 0 0; 0 0; 0 1];
        end
        duc = (~isempty(strfind(arbConfig.model, 'DUC')));
        if (duc)
            ud(:,1) = (ud(:,1) + ud(:,2)) ~= 0;
            ud(:,2) = ud(:,1);
        else
            idx = find(ud(:,1) .* ud(:,2));
            idx1 = idx;
            idx1(mod(idx,2)~=0) = [];
            ud(idx1,1) = 0;
            idx2 = idx;
            idx2(mod(idx,2)==0) = [];
            ud(idx2,2) = 0;
        end
        if (strcmp(type, 'single'))
            ud(:,1) = ud(:,1) + ud(:,2);
            ud(:,2) = 0;
        end
        set(pb, 'UserData', ud);
        set(pb, 'String', iqchannelsetup('mkstring', ud, arbConfig, type));
    % convert into a string that is displayed on the download pushbutton
    case 'mkstring'
        if (~isempty(pb))
            if (strcmp(type, 'single'))
                pre = '';
            else
                pre = 'I to ';
            end
            chs1 = find(pb(:,1));
            if (isempty(chs1))
                str = '';
            else
                str = sprintf('%sCh%s', pre, sprintf('%d+', chs1));
                str(end) = [];
            end
            if (~strcmp(pre, ''))
                chs2 = find(pb(:,2));
                if (~isempty(chs2))
                    if (~isempty(chs1))
                        str = sprintf('%s, ', str);
                    end
                    str = sprintf('%sQ to Ch%s', str, sprintf('%d+', chs2));
                    str(end) = [];
                end
            end
            str = sprintf('%s...', str);
        else
            str = '...';
        end
    % convert into a string for the "Generate MATLAB code" function
    case 'arraystring'
        str = '[';
        for i=1:size(pb,1)
            str = sprintf('%s%s', str, sprintf('%d ', pb(i,:)));
            str(end) = [];
            if (i ~= size(pb,1))
                str = sprintf('%s; ', str);
            end
        end
        str = sprintf('%s]', str);
end

